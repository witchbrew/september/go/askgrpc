// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.12.2
// source: ask.proto

package askgrpc

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type AskEmpty struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *AskEmpty) Reset() {
	*x = AskEmpty{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ask_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AskEmpty) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AskEmpty) ProtoMessage() {}

func (x *AskEmpty) ProtoReflect() protoreflect.Message {
	mi := &file_ask_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AskEmpty.ProtoReflect.Descriptor instead.
func (*AskEmpty) Descriptor() ([]byte, []int) {
	return file_ask_proto_rawDescGZIP(), []int{0}
}

type CreateQuestion struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Text    string   `protobuf:"bytes,1,opt,name=text,proto3" json:"text,omitempty"`
	Options []string `protobuf:"bytes,2,rep,name=options,proto3" json:"options,omitempty"`
}

func (x *CreateQuestion) Reset() {
	*x = CreateQuestion{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ask_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateQuestion) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateQuestion) ProtoMessage() {}

func (x *CreateQuestion) ProtoReflect() protoreflect.Message {
	mi := &file_ask_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateQuestion.ProtoReflect.Descriptor instead.
func (*CreateQuestion) Descriptor() ([]byte, []int) {
	return file_ask_proto_rawDescGZIP(), []int{1}
}

func (x *CreateQuestion) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

func (x *CreateQuestion) GetOptions() []string {
	if x != nil {
		return x.Options
	}
	return nil
}

type GetQuestion struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ID      string   `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Text    string   `protobuf:"bytes,2,opt,name=text,proto3" json:"text,omitempty"`
	Options []string `protobuf:"bytes,3,rep,name=options,proto3" json:"options,omitempty"`
}

func (x *GetQuestion) Reset() {
	*x = GetQuestion{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ask_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetQuestion) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetQuestion) ProtoMessage() {}

func (x *GetQuestion) ProtoReflect() protoreflect.Message {
	mi := &file_ask_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetQuestion.ProtoReflect.Descriptor instead.
func (*GetQuestion) Descriptor() ([]byte, []int) {
	return file_ask_proto_rawDescGZIP(), []int{2}
}

func (x *GetQuestion) GetID() string {
	if x != nil {
		return x.ID
	}
	return ""
}

func (x *GetQuestion) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

func (x *GetQuestion) GetOptions() []string {
	if x != nil {
		return x.Options
	}
	return nil
}

type QuestionMeta struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ID      string `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
	Options int32  `protobuf:"varint,2,opt,name=options,proto3" json:"options,omitempty"`
}

func (x *QuestionMeta) Reset() {
	*x = QuestionMeta{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ask_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *QuestionMeta) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*QuestionMeta) ProtoMessage() {}

func (x *QuestionMeta) ProtoReflect() protoreflect.Message {
	mi := &file_ask_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use QuestionMeta.ProtoReflect.Descriptor instead.
func (*QuestionMeta) Descriptor() ([]byte, []int) {
	return file_ask_proto_rawDescGZIP(), []int{3}
}

func (x *QuestionMeta) GetID() string {
	if x != nil {
		return x.ID
	}
	return ""
}

func (x *QuestionMeta) GetOptions() int32 {
	if x != nil {
		return x.Options
	}
	return 0
}

type AskIDWrapper struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ID string `protobuf:"bytes,1,opt,name=ID,proto3" json:"ID,omitempty"`
}

func (x *AskIDWrapper) Reset() {
	*x = AskIDWrapper{}
	if protoimpl.UnsafeEnabled {
		mi := &file_ask_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AskIDWrapper) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AskIDWrapper) ProtoMessage() {}

func (x *AskIDWrapper) ProtoReflect() protoreflect.Message {
	mi := &file_ask_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AskIDWrapper.ProtoReflect.Descriptor instead.
func (*AskIDWrapper) Descriptor() ([]byte, []int) {
	return file_ask_proto_rawDescGZIP(), []int{4}
}

func (x *AskIDWrapper) GetID() string {
	if x != nil {
		return x.ID
	}
	return ""
}

var File_ask_proto protoreflect.FileDescriptor

var file_ask_proto_rawDesc = []byte{
	0x0a, 0x09, 0x61, 0x73, 0x6b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x0a, 0x0a, 0x08, 0x41,
	0x73, 0x6b, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x3e, 0x0a, 0x0e, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x65, 0x78,
	0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65, 0x78, 0x74, 0x12, 0x18, 0x0a,
	0x07, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x09, 0x52, 0x07,
	0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x22, 0x4b, 0x0a, 0x0b, 0x47, 0x65, 0x74, 0x51, 0x75,
	0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x0e, 0x0a, 0x02, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x49, 0x44, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x65, 0x78, 0x74, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x74, 0x65, 0x78, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x6f, 0x70,
	0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x70, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x22, 0x38, 0x0a, 0x0c, 0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e,
	0x4d, 0x65, 0x74, 0x61, 0x12, 0x0e, 0x0a, 0x02, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x49, 0x44, 0x12, 0x18, 0x0a, 0x07, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x05, 0x52, 0x07, 0x6f, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x22, 0x1e,
	0x0a, 0x0c, 0x41, 0x73, 0x6b, 0x49, 0x44, 0x57, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x12, 0x0e,
	0x0a, 0x02, 0x49, 0x44, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x49, 0x44, 0x32, 0xa8,
	0x01, 0x0a, 0x03, 0x41, 0x73, 0x6b, 0x12, 0x2a, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x12, 0x0f, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f,
	0x6e, 0x1a, 0x0d, 0x2e, 0x41, 0x73, 0x6b, 0x49, 0x44, 0x57, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72,
	0x22, 0x00, 0x12, 0x24, 0x0a, 0x03, 0x47, 0x65, 0x74, 0x12, 0x0d, 0x2e, 0x41, 0x73, 0x6b, 0x49,
	0x44, 0x57, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x1a, 0x0c, 0x2e, 0x47, 0x65, 0x74, 0x51, 0x75,
	0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x00, 0x12, 0x29, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x4d,
	0x65, 0x74, 0x61, 0x12, 0x0d, 0x2e, 0x41, 0x73, 0x6b, 0x49, 0x44, 0x57, 0x72, 0x61, 0x70, 0x70,
	0x65, 0x72, 0x1a, 0x0d, 0x2e, 0x51, 0x75, 0x65, 0x73, 0x74, 0x69, 0x6f, 0x6e, 0x4d, 0x65, 0x74,
	0x61, 0x22, 0x00, 0x12, 0x24, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x0d, 0x2e,
	0x41, 0x73, 0x6b, 0x49, 0x44, 0x57, 0x72, 0x61, 0x70, 0x70, 0x65, 0x72, 0x1a, 0x09, 0x2e, 0x41,
	0x73, 0x6b, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22, 0x00, 0x42, 0x0b, 0x5a, 0x09, 0x2e, 0x3b, 0x61,
	0x73, 0x6b, 0x67, 0x72, 0x70, 0x63, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_ask_proto_rawDescOnce sync.Once
	file_ask_proto_rawDescData = file_ask_proto_rawDesc
)

func file_ask_proto_rawDescGZIP() []byte {
	file_ask_proto_rawDescOnce.Do(func() {
		file_ask_proto_rawDescData = protoimpl.X.CompressGZIP(file_ask_proto_rawDescData)
	})
	return file_ask_proto_rawDescData
}

var file_ask_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_ask_proto_goTypes = []interface{}{
	(*AskEmpty)(nil),       // 0: AskEmpty
	(*CreateQuestion)(nil), // 1: CreateQuestion
	(*GetQuestion)(nil),    // 2: GetQuestion
	(*QuestionMeta)(nil),   // 3: QuestionMeta
	(*AskIDWrapper)(nil),   // 4: AskIDWrapper
}
var file_ask_proto_depIdxs = []int32{
	1, // 0: Ask.Create:input_type -> CreateQuestion
	4, // 1: Ask.Get:input_type -> AskIDWrapper
	4, // 2: Ask.GetMeta:input_type -> AskIDWrapper
	4, // 3: Ask.Delete:input_type -> AskIDWrapper
	4, // 4: Ask.Create:output_type -> AskIDWrapper
	2, // 5: Ask.Get:output_type -> GetQuestion
	3, // 6: Ask.GetMeta:output_type -> QuestionMeta
	0, // 7: Ask.Delete:output_type -> AskEmpty
	4, // [4:8] is the sub-list for method output_type
	0, // [0:4] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_ask_proto_init() }
func file_ask_proto_init() {
	if File_ask_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_ask_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AskEmpty); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ask_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateQuestion); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ask_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetQuestion); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ask_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*QuestionMeta); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_ask_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AskIDWrapper); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_ask_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_ask_proto_goTypes,
		DependencyIndexes: file_ask_proto_depIdxs,
		MessageInfos:      file_ask_proto_msgTypes,
	}.Build()
	File_ask_proto = out.File
	file_ask_proto_rawDesc = nil
	file_ask_proto_goTypes = nil
	file_ask_proto_depIdxs = nil
}
