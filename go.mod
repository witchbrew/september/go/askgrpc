module gitlab.com/witchbrew/september/go/askgrpc

go 1.15

require (
	github.com/golang/protobuf v1.4.2
	github.com/stretchr/testify v1.6.1
	google.golang.org/genproto v0.0.0-20200925023002-c2d885f95484 // indirect
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.24.0
)
