package askgrpc

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
	"net"
	"testing"
)

const port = ":50051"

type mockServer struct {
	mock.Mock
	UnimplementedAskServer
}

func (m *mockServer) Create(ctx context.Context, question *CreateQuestion) (*IDWrapper, error) {
	args := m.Called(ctx, question)
	return args.Get(0).(*IDWrapper), args.Error(1)
}

func (m *mockServer) Get(ctx context.Context, wrapper *IDWrapper) (*GetQuestion, error) {
	args := m.Called(ctx, wrapper)
	return args.Get(0).(*GetQuestion), args.Error(1)
}

func (m *mockServer) GetMeta(ctx context.Context, wrapper *IDWrapper) (*QuestionMeta, error) {
	args := m.Called(ctx, wrapper)
	return args.Get(0).(*QuestionMeta), args.Error(1)
}

func (m *mockServer) Delete(ctx context.Context, wrapper *IDWrapper) (*Empty, error) {
	args := m.Called(ctx, wrapper)
	return args.Get(0).(*Empty), args.Error(1)
}

func startServer(t *testing.T, s *mockServer) *grpc.Server {
	listener, err := net.Listen("tcp", port)
	require.Nil(t, err)
	grpcServer := grpc.NewServer()
	RegisterAskServer(grpcServer, s)
	go func() {
		err := grpcServer.Serve(listener)
		require.Nil(t, err)
	}()
	return grpcServer
}

func newClient(t *testing.T) AskClient {
	clientConn, err := grpc.Dial(fmt.Sprintf("localhost%s", port), grpc.WithInsecure(), grpc.WithBlock())
	require.Nil(t, err)
	return NewAskClient(clientConn)
}

func TestAskClient_Create(t *testing.T) {
	q := &CreateQuestion{
		Text: "hello world",
	}
	m := &mockServer{}
	s := startServer(t, m)
	defer s.Stop()
	c := newClient(t)
	idWrapper := &IDWrapper{ID: "yo"}
	m.On("Create", mock.Anything, mock.IsType(&CreateQuestion{})).Return(idWrapper, nil)
	idWrapper2, err := c.Create(context.Background(), q)
	require.Nil(t, err)
	require.Equal(t, idWrapper.ID, idWrapper2.ID)
}

func TestAskClient_Get(t *testing.T) {
	q := &GetQuestion{
		ID:      "yo",
		Text:    "hello world",
		Options: []string{"one", "two"},
	}
	m := &mockServer{}
	s := startServer(t, m)
	defer s.Stop()
	c := newClient(t)
	m.On("Get", mock.Anything, mock.IsType(&IDWrapper{})).Return(q, nil)
	q2, err := c.Get(context.Background(), &IDWrapper{ID: q.ID})
	require.Nil(t, err)
	require.Equal(t, q.ID, q2.ID)
	require.Equal(t, q.Text, q2.Text)
	require.Equal(t, q.Options, q2.Options)
}

func TestAskClient_GetMeta(t *testing.T) {
	qm := &QuestionMeta{
		ID:      "yo",
		Options: 2,
	}
	m := &mockServer{}
	s := startServer(t, m)
	defer s.Stop()
	c := newClient(t)
	m.On("GetMeta", mock.Anything, mock.IsType(&IDWrapper{})).Return(qm, nil)
	qm2, err := c.GetMeta(context.Background(), &IDWrapper{ID: qm.ID})
	require.Nil(t, err)
	require.Equal(t, qm.ID, qm2.ID)
	require.Equal(t, qm.Options, qm2.Options)
}


func TestAskClient_Delete(t *testing.T) {
	id := "yo"
	m := &mockServer{}
	s := startServer(t, m)
	defer s.Stop()
	c := newClient(t)
	m.On("Delete", mock.Anything, mock.IsType(&IDWrapper{})).Return(&Empty{}, nil)
	_, err := c.Delete(context.Background(), &IDWrapper{ID: id})
	require.Nil(t, err)
}
